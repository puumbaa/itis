package graph2;

import java.util.*;

public class GraphTasks2Solution implements GraphTasks2 {

    @Override
    public HashMap<Integer, Integer> dijkstraSearch(int[][] adjacencyMatrix, int startIndex) {
        boolean[] vertexCheck = new boolean[adjacencyMatrix.length];
        HashMap<Integer, Integer> hashMap = new HashMap<>();
        vertexCheck[startIndex] = true;
        for (int j = 0; j < adjacencyMatrix.length; j++) {
            if (j != startIndex) {
                if (adjacencyMatrix[startIndex][j] == 0) hashMap.put(j, Integer.MAX_VALUE);
                else hashMap.put(j, adjacencyMatrix[startIndex][j]);
            }
        }
        for (int x : hashMap.keySet()) {
            for (int j = 0; j < adjacencyMatrix.length; j++) {
                if (adjacencyMatrix[x][j] == 0) continue;
                if (j != x && !vertexCheck[j] && adjacencyMatrix[x][j] + hashMap.get(x) < hashMap.get(j)) {
                    hashMap.put(j, adjacencyMatrix[x][j] + hashMap.get(x));
                    vertexCheck[j] = true;
                }
            }
        }
        return hashMap;
    }


    @Override
    public Integer primaAlgorithm(int[][] adjacencyMatrix) {
        HashSet<Integer> vertexInSet = new HashSet<>();
        int sum = 0;
// ~~~~~~~~~~~~~~~~~~~~~~~  Первый шаг (для 1ой вершины)  ~~~~~~~~~~~~~~~~~~~~~~~~~~
        vertexInSet.add(0);
        int min = adjacencyMatrix[0][1];
        int indexOfMin = 1;
        for (int i = 1; i < adjacencyMatrix.length; i++)
            if (adjacencyMatrix[0][i] < min && adjacencyMatrix[0][i] > 0) {
                min = adjacencyMatrix[0][i];
                indexOfMin = i;
            }
        vertexInSet.add(indexOfMin);
        sum += min;
// ---------------------------------------------------------------------------------
        for (int i = 0; i < adjacencyMatrix.length - 2; i++) {
            int queriedIndex = 0;
            int minValue = Integer.MAX_VALUE;
            for (Integer x : vertexInSet) {
                for (int j = 0; j < adjacencyMatrix.length; j++) {
                    if (adjacencyMatrix[x][j] < minValue && adjacencyMatrix[x][j] > 0 && !vertexInSet.contains(j)) {
                        minValue = adjacencyMatrix[x][j];
                        queriedIndex = j;
                    }
                }
            }
            vertexInSet.add(queriedIndex);
            sum += minValue;
        }
        return sum;
    }

    @Override
    public Integer kraskalAlgorithm(int[][] adjacencyMatrix) {
        List<Set<Integer>> sets = new ArrayList<>();
        TreeMap<Integer, List<Set<Integer>>> treeMap = new TreeMap<>();
        int key;

        for (int i = 0; i < adjacencyMatrix.length; i++) {     // отражение матрицы весов в мапу (Работает правильно)
            for (int j = 0; j < adjacencyMatrix.length; j++) {
                Set<Integer> pair = new HashSet<>();
                key = adjacencyMatrix[i][j];
                if (key > 0) {
                    pair.add(i);
                    pair.add(j);

                    if (!treeMap.containsKey(key)) treeMap.put(key, new ArrayList<>());

                    boolean check = true;
                    for (Set<Integer> setForCheck : treeMap.get(key))
                        if (setForCheck.containsAll(pair)) check = false;
                    if (check) treeMap.get(key).add(pair);
                }
            }
        }


        int sum = 0;
        for (Integer r : treeMap.keySet()) { // урааа оно работает!!!!!!!!
            int firstEntryIndex = -1;
            int secondEntryIndex = -1;

            for (Set<Integer> valueSet : treeMap.get(r)) {
                if (sets.size() == 0) {
                    sets.add(new HashSet<>(valueSet));
                    sum += r;
                    continue;
                }
                Object[] objects = valueSet.toArray();
                for (int i = 0; i < objects.length; i++) {
                    for (int k = 0; k < sets.size(); k++) {
                        if (sets.get(k).contains((Integer) objects[i])) {
                            if (i == 0) firstEntryIndex = k;
                            if (i == 1) secondEntryIndex = k;
                        }

                    }
                }
                if (firstEntryIndex == secondEntryIndex && firstEntryIndex >= 0) continue;

                if (firstEntryIndex >= 0 && secondEntryIndex < 0) sets.get(firstEntryIndex).addAll(valueSet);

                if (secondEntryIndex >= 0 && firstEntryIndex < 0) sets.get(secondEntryIndex).addAll(valueSet);

                if (firstEntryIndex >= 0 && secondEntryIndex >= 0) {
                    sets.get(firstEntryIndex).addAll(sets.get(secondEntryIndex));
                    sets.get(secondEntryIndex).clear();
                }
                if (firstEntryIndex < 0 && secondEntryIndex < 0) sets.add(valueSet);
                firstEntryIndex = -1;
                secondEntryIndex = -1;
                sum += r;
            }
        }
        return sum;
    }
}
