package unit_annotations;

import java.util.List;

public class Warrior implements Unit {

    public Unit getSensei() {
        return Sensei;
    }

    public void setSensei(Warrior sensei) {
        Sensei = sensei;
    }

    public List<Unit> getTeamMates() {
        return teamMates;
    }

    public void setTeamMates(List<Unit> teamMates) {
        this.teamMates = teamMates;
    }

    @SetListOfUnits(count = 4, unitsClass = "unit_annotations.Warrior")
    private List<Unit> teamMates;

    @Randomize(min = 3, max = 18)
    private int age;

    @SetUnit
    private Warrior Sensei;


    @UseSetter("stranger")
    private String name;


    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Warrior{" +
                "teamMates=" + teamMates +
                ", age=" + age +
                ", Sensei=" + Sensei +
                ", name='" + name + '\'' +
                '}';
    }

    public void setName(String name) {
        this.name = name;
    }


    @Override
    public void doSomething() {
        System.out.println("тыщ");
    }
}
