package unit_annotations;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class UnitFactory {

    boolean UnitAlreadyCreated = false;
    boolean ListOfUnitsAlreadyCreated = false;

    public static void main(String[] args) throws NoSuchFieldException {
        UnitFactory unitFactory = new UnitFactory();
        System.out.println(unitFactory.createUnitByName(Warrior.class.getName()));
    }

    public Unit createUnitByName(String unitClass) {
        try {
            Class<?> aClass = Class.forName(unitClass);
            if (!Unit.class.isAssignableFrom(aClass)) throw new IllegalArgumentException("class is not Unit");

            else {
                Unit o = (Unit) aClass.getConstructor().newInstance();
                for (Field declaredField : aClass.getDeclaredFields()) {
                    randomize(declaredField, o);
                    useSetter(declaredField, o);
                    if (!UnitAlreadyCreated) setUnit(declaredField, o);

                    if (!ListOfUnitsAlreadyCreated) setListOfUnits(declaredField, o);
                }

                return o;
            }

        } catch (ClassNotFoundException | NoSuchMethodException | InstantiationException
                | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void randomize(Field field, Object o) throws IllegalAccessException {
        if (field.isAnnotationPresent(Randomize.class)) {
            Randomize annotation = field.getAnnotation(Randomize.class);
            field.setAccessible(true);
            field.set(o, annotation.min() + new Random().nextInt(annotation.max() - annotation.min()));
        }
    }

    private void useSetter(Field field, Object o) throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        if (field.isAnnotationPresent(UseSetter.class)) {
            Method method = o.getClass().getDeclaredMethod("set" + firstUpperCase(field.getName()), field.getType());
            String value = field.getAnnotation(UseSetter.class).value();
            switch (field.getType().getName()) {
                case "int", "Integer" -> method.invoke(o, Integer.parseInt(value));
                case "double", "Double" -> method.invoke(o, Double.parseDouble(value));
                case "float", "Float" -> method.invoke(o, Float.parseFloat(value));
                case "char", "Character" -> method.invoke(o, value.charAt(0));
                case "byte", "Byte" -> method.invoke(o, Byte.parseByte(value));
                case "short", "Short" -> method.invoke(o, Short.parseShort(value));
                case "String" -> method.invoke(o, value);

            }
        }
    }


    private void setUnit(Field field, Object o) throws IllegalAccessException {
        if (field.isAnnotationPresent(SetUnit.class)) {
            field.setAccessible(true);
            UnitAlreadyCreated = true;
            field.set(o, createUnitByName(field.getType().getName()));
        }
    }

    private List<Unit> getUnits(int count, String unitClass) {
        List<Unit> units = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            units.add(createUnitByName(unitClass));
        }
        return units;
    }

    private void setListOfUnits(Field field, Object o) throws IllegalAccessException {
        if (field.isAnnotationPresent(SetListOfUnits.class)) {
            SetListOfUnits annotation = field.getAnnotation(SetListOfUnits.class);
            field.setAccessible(true);
            ListOfUnitsAlreadyCreated = true;
            field.set(o, getUnits(annotation.count(), annotation.unitsClass()));
        }
    }

    private String firstUpperCase(String word) {
        if (word == null || word.isEmpty()) return word;
        return word.substring(0, 1).toUpperCase() + word.substring(1);
    }
}
