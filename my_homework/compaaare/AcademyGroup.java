package compaaare;

public class AcademyGroup {
    private Integer number;
    private Double rating;
    private Integer cntOfStudents;

    @Override
    public String toString() {
        return "AcademyGroup{" +
                "number=" + number +
                ", rating=" + rating +
                ", cntOfStudents=" + cntOfStudents +
                '}';
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public Integer getCntOfStudents() {
        return cntOfStudents;
    }

    public void setCntOfStudents(Integer cntOfStudents) {
        this.cntOfStudents = cntOfStudents;
    }

    public AcademyGroup(Integer number, Double rating, Integer cntOfStudents) {
        this.number = number;
        this.rating = rating;
        this.cntOfStudents = cntOfStudents;
    }
}
