package compaaare;

public class Student {
    public AcademyGroup getGroup() {
        return group;
    }

    private final AcademyGroup group;
    private String firstName; // а как потом использовать это при сортировке ?
    private String lastName;
    private Integer age;


    public Student(AcademyGroup group, String firstName, String lastName, Integer age) {
        this.group = group;
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }

    @Override
    public String toString() {
        return "Student{" +
                "group=" + group +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                '}';
    }


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }


}
