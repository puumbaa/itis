package compaaare;

import java.util.Comparator;

public class StudentByGroupNumberComparator implements Comparator<Student> {

    @Override
    public int compare(Student o1, Student o2) {
        return o1.getGroup().getNumber().compareTo(o2.getGroup().getNumber());
    }
}
