package compaaare;

import java.util.Comparator;

public class StudentsByGroupRatingComparator implements Comparator<Student> {
    @Override
    public int compare(Student o1, Student o2) {
        return o2.getGroup().getRating().compareTo(o1.getGroup().getRating());
    }
}
