package compaaare;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        Student student1 = new Student(new AcademyGroup(3, (double) 90, 28),
                "Alex", "Jons", 18);

        Student student2 = new Student(new AcademyGroup(1, (double) 100, 30),
                "Blex", "Jons", 19);

        ArrayList<Student> arr = new ArrayList<>();
        {
            arr.add(student1);
            arr.add(student2);
        }
        System.out.println(" 1ый Начальный " + arr);
        System.out.println();

        System.out.println("Первая сортировка");
        arr.sort(new StudentByGroupNumberComparator());
        System.out.println(arr);

        System.out.println("Очистка");
        arr.clear();
        System.out.println(arr);
        System.out.println();
        {
            arr.add(student2);
            arr.add(student1);
        }
        System.out.println("2-oй начальный ");
        System.out.println(arr);
        System.out.println();
        System.out.println("Вторая сортировка");
        arr.sort(new StudentsByFirstNameComparator());
        System.out.println(arr);

    }
}
