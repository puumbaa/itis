package weather__27_03;

public class Hour {
    private String timestamp;
    private Double temperature;
    private Double relativeHumidity;
    private Double windSpeed;
    private Double windDirection;
    private String[] timestampSplit;

    @Override
    public String toString() {
        return "Day{" +
                "timestamp='" + timestamp + '\'' +
                ", temperature=" + temperature +
                ", relativeHumidity=" + relativeHumidity +
                ", windSpeed=" + windSpeed +
                ", windDirection=" + windDirection +
                '}';
    }

    public String getTimestamp() {
        return timestamp;
    }



    public Double getTemperature() {
        return temperature;
    }



    public Double getRelativeHumidity() {
        return relativeHumidity;
    }


    public Double getWindSpeed() {
        return windSpeed;
    }


    public Double getWindDirection() {
        return windDirection;
    }


    public Hour(String[] data) {
        this.timestamp = data[0];
        this.timestampSplit = timestamp.split("T");
        this.temperature = Double.parseDouble(data[1]);
        this.relativeHumidity = Double.parseDouble(data[2]);
        this.windSpeed = Double.parseDouble(data[3]);
        this.windDirection = Double.parseDouble(data[4]);

    }

    static public String showData(String s) {
        String res = "";
        res = res + (s.charAt(6) + "" + s.charAt(7) + "." + s.charAt(4) + s.charAt(5) + "." + s.charAt(0) + s.charAt(1) + s.charAt(2) + s.charAt(3));
        return res;
    }

    static public String showTime(String s) {
        String res = "";
        res += s.charAt(0) + "" + s.charAt(1) + ":" + s.charAt(2) + "" + s.charAt(3);
        return res;
    }

    public int getHour() {
        return Integer.parseInt("" + timestampSplit[1].charAt(0) + timestampSplit[1].charAt(1));
    }

    public int getDay() {
        return Integer.parseInt(timestampSplit[0].charAt(6) + "" + timestampSplit[0].charAt(7));
    }

    public int getMonth() {
        return Integer.parseInt(timestampSplit[0].charAt(4) + "" + timestampSplit[0].charAt(5));
    }

    public int getYear() {
        return Integer.parseInt("" + timestampSplit[0].charAt(0) + timestampSplit[0].charAt(1) + timestampSplit[0].charAt(2) + timestampSplit[0].charAt(3));
    }

}
