package weather__27_03;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class WeatherManager {
    List<Hour> hourList = new ArrayList<>();
    int[] cardinalPoints = new int[8];

    public static void main(String[] args) throws IOException {
        WeatherManager wm = new WeatherManager();
        File inputFile = new File("my_homework\\weather__27_03\\dataexport_20210320T064822.csv");
        wm.getInfo(inputFile);
    }

    void getInfo(File inputFile) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(inputFile));
        File outputFile = new File("my_homework\\weather__27_03\\weather_info.csv");
        BufferedWriter bw = new BufferedWriter(new FileWriter(outputFile));
        String line;
        long strNum = 0;
        while ((line = br.readLine()) != null) {
            strNum++;
            if (strNum > 10) {
                hourList.add(new Hour(line.split(",")));
            }
        }
        br.close();
        bw.write(
                "Средняя температура: " + getAverageValue(hourList, "temp") + "\n"
                        + "Средняя влажность: " + getAverageValue(hourList, "humidity") + "\n"
                        + "Средняя скорость ветра: " + getAverageValue(hourList, "wind") + "\n"
                        + "День и час когда была самая высокая температура: " + findHourWithMaxTemperature(hourList) + "\n"
                        + "День и час когда была самая низкая влажность: " + findHourWithMinHumidity(hourList) + "\n"
                        + "День и час когда была самая высокая скорость ветра: " + findHourWithStrongestWind(hourList) + "\n"
                        + "Самое частое напраление ветра: " + findMostFrequentWindDirection(hourList));
        bw.close();
    }

    Double getAverageValue(List<Hour> hourList, String value) {
        Double sum = 0.0;
        for (Hour hour : hourList) {
            if (value.equals("temp")) sum += hour.getTemperature();
            if (value.equals("humidity")) sum += hour.getRelativeHumidity();
            if (value.equals("wind")) sum += hour.getWindSpeed();
        }
        return sum / hourList.size();
    }

    String findHourWithMaxTemperature(List<Hour> hourList) {
        if (hourList.size() <= 0) return "error in method findHourWithMaxTemperature: hourList.size() <= 0";
        Hour maxValueHour = hourList.get(0);
        for (Hour hour : hourList) {
            if (hour.getTemperature() > maxValueHour.getTemperature()) maxValueHour = hour;
        }
        String[] info = maxValueHour.getTimestamp().split("T");
        return "дата: " + Hour.showData(info[0]) + " время: " + Hour.showTime(info[1]) +
                " Значение: " + maxValueHour.getTemperature();
    }

    String findHourWithMinHumidity(List<Hour> hourList) {
        if (hourList.size() <= 0) return "error in method findHourWithMinHumidity: hourList.size() <= 0";
        Hour minValueHour = hourList.get(0);
        for (Hour hour : hourList) {
            if (hour.getRelativeHumidity() < minValueHour.getRelativeHumidity()) minValueHour = hour;
        }
        String[] info = minValueHour.getTimestamp().split("T");
        return "дата: " + Hour.showData(info[0]) + " время: " + Hour.showTime(info[1]) +
                " Значение: " + minValueHour.getRelativeHumidity();
    }

    String findHourWithStrongestWind(List<Hour> hourList) {
        if (hourList.size() <= 0) return "error in method findHourWithStrongestWind: hourList.size() <= 0";
        Hour maxValueHour = hourList.get(0);
        for (Hour hour : hourList) {
            if (hour.getWindSpeed() > maxValueHour.getWindSpeed()) maxValueHour = hour;
        }
        String[] info = maxValueHour.getTimestamp().split("T");
        return "дата: " + Hour.showData(info[0]) + " время: " + Hour.showTime(info[1]) +
                " Значение: " + maxValueHour.getWindSpeed();
    }

    String findMostFrequentWindDirection(List<Hour> hourList) {

        double max = -1.0;
        int ind = 0;
        for (Hour hour : hourList) {
            double x = hour.getWindDirection();
            if (x > 337.5 && x <= 22.5) cardinalPoints[0]++; //North
            if (x > 22.5 && x <= 67.5) cardinalPoints[1]++; //North-East
            if (x > 67.5 && x <= 112.5) cardinalPoints[2]++; //East
            if (x > 112.5 && x <= 157.5) cardinalPoints[3]++; //East-South
            if (x > 157.5 && x <= 202.5) cardinalPoints[4]++; //South
            if (x > 202.5 && x <= 247.5) cardinalPoints[5]++; //South-West
            if (x > 247.5 && x <= 292.5) cardinalPoints[6]++; //West
            if (x > 292.5 && x <= 337.5) cardinalPoints[7]++; //West-North
        }
        for (int i = 0; i < 8; i++) {
            if (cardinalPoints[i] > max) {
                max = cardinalPoints[i];
                ind = i;
            }
        }
        return switch (ind) {
            case 0 -> "North";
            case 1 -> "North-East";
            case 2 -> "East";
            case 3 -> "East-South";
            case 4 -> "South";
            case 5 -> "South-West";
            case 6 -> "West";
            case 7 -> "West-North";
            default -> null;
        };
    }

}
