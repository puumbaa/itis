package functional_interfacies_08_04;

import java.util.ArrayDeque;
import java.util.Queue;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Queue<Integer> queue = new ArrayDeque<>();
        CircularLinkedList circularLinkedList = new CircularLinkedList();
        int n = sc.nextInt();
        int k;

        for (int i = 0; i < n; i++) {
            k = sc.nextInt();
            int counter = 0;


            boolean flag = true;
            for (int j = 0; j < k; j++) {
                circularLinkedList.addNode(sc.nextInt());
            }
            Node a = circularLinkedList.getFirst();
            Node b = a.nextNode;

            while (flag) {
                if (circularLinkedList.getSize() == 0) break;
                if (arePrimes(a, b)) {
                    counter = 0;
                    queue.add(a.nextNode.index + 1);
                    a.deleteNext();
                    circularLinkedList.minusSize();
                } else {
                    counter++;
                    if (counter == circularLinkedList.getSize()) flag = false;
                }

                a = a.nextNode;
                b = a.nextNode;
            }
            System.out.print(queue.size() + " ");
            for (int x : queue) System.out.print(x + " ");
            System.out.println();
            queue.clear();
            circularLinkedList.clear();

        }


    }

    static boolean arePrimes(Node x, Node y) {
        int d1 = x.value;
        int d2 = y.value;
        while (d1 != d2) {
            if (d1 > d2) {
                d1 -= d2;
            } else {
                d2 -= d1;
            }
        }
        return d1 == 1;
    }
}

class CircularLinkedList {

    private int size = 0;
    private Node head = null;
    private Node tail = null;


    public int getSize() {
        return size;
    }

    public void addNode(int value) {

        Node newNode = new Node(value, size);

        if (head == null) {
            head = newNode;
        } else {
            tail.nextNode = newNode;
        }
        size++;
        tail = newNode;
        tail.nextNode = head;
    }

    public void clear() {
        size = 0;
        head = null;
        tail = null;
    }

    public Node get(int index) {
        if (index > size || index < 0) throw new IllegalArgumentException();
        Node currentNode = head;
        for (int i = 0; i != index; i++) {
            currentNode = currentNode.nextNode;
        }
        return currentNode;
    }

    public Node getFirst() {
        return head;
    }


    public void minusSize() {
        size--;
    }
}


class Node {
    int index;
    int value;
    Node nextNode;

    public Node(int value, int index) {
        this.value = value;
        this.index = index;
    }

    public void deleteNext() {
        nextNode = nextNode.nextNode;
    }
}