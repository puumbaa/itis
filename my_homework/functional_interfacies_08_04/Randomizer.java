package functional_interfacies_08_04;

import java.util.function.Supplier;

public class Randomizer {

    public static void main(String[] args) {
        Supplier<Integer> supplier = () -> {
            long time = System.currentTimeMillis();
            return (int) ((time % 100 * time % 10) % 10);
        };
        System.out.println(supplier.get());
    }
}
