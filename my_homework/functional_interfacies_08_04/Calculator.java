package functional_interfacies_08_04;

import java.util.HashMap;
import java.util.Scanner;
import java.util.function.BinaryOperator;

public class Calculator {
    static HashMap<String, BinaryOperator<Integer>> map = new HashMap<>();

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int x = in.nextInt();
        int y = in.nextInt();
        String str = in.next();
        map.put("+", Integer::sum);
        map.put("-", (a, b) -> a - b);
        map.put("*", (a, b) -> a * b);
        map.put("/", (a, b) -> a / b);
        map.put("^", (a, b) -> {
            while (b > 1) {
                a *= a;
                b--;
            }
            return a;
        });
        System.out.println(Operation(x, y, str));
    }

    public static int Operation(int x, int y, String v) {
        return map.get(v).apply(x, y);
    }

}