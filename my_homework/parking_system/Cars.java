package parking_system;

public class Cars {
    private int moves;
    private final int number;
    private final String type;


    public Cars(int moves,int number,String type){
        this.moves = moves;
        this.number = number;
        this.type = type;
    }


    public int getMoves() {
        return moves;
    }

    public void setMoves(int moves) {
        this.moves = moves;
    }

    public int getNumber() {
        return number;
    }


    public String getType() {
        return type;
    }
}
