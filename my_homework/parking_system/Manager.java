package parking_system;
import java.util.Arrays;
import java.util.Scanner;

public class Manager {
    private static int minMovesForCars;
    private static int minMovesForTrucks;
    private static int cntOfNewCars;
    private static int cntOfNewTrucks;

    public static void main(String[] args) {
        System.out.println("Enter the parking capacity:");
        Scanner sc = new Scanner(System.in);
        Park newPark = new Park(sc.nextInt());
        int carsCapacity = 1 + (int) (Math.random() * (newPark.getCapacity()-2));
        int trucksCapacity = newPark.getCapacity()-carsCapacity;
        Cars[] CarsOnPark = new Cars[carsCapacity];
        Cars[] TrucksOnPark = new Cars[trucksCapacity];
        minMovesForCars = 11;
        minMovesForTrucks=11;
        boolean letsGo = true;

// ~~~~~~~~~~~~~~~~~~~~~ В ы п о л н е н и е    х о д а ~~~~~~~~~~~~~~~~~~~~~~
        while (letsGo) {
            minMovesForCars = checkMovesAndGetMinOfThem(CarsOnPark, minMovesForCars,"car");
            minMovesForTrucks = checkMovesAndGetMinOfThem(TrucksOnPark, minMovesForTrucks,"truck");
            cntOfNewCars = 1 + (int) (Math.random() * (newPark.getCapacity() / 3));
            cntOfNewTrucks = 1 + (int) (Math.random() * (newPark.getCapacity() / 3));
            takePlace(CarsOnPark,  "car");
            takePlace(TrucksOnPark, "truck");


            for (int i = 0; i < carsCapacity - 1; i++) {
                if (CarsOnPark[i] == null && CarsOnPark[i + 1] == null && cntOfNewTrucks > 0 && cntOfNewCars ==0) {

                    CarsOnPark[i] = new Cars(
                            1 + (int) (Math.random() * 10),
                            1000 + (int) (Math.random() * 8999),
                            "truck");
                    CarsOnPark[i + 1] = CarsOnPark[i];

                    if(CarsOnPark[i].getMoves()< minMovesForCars) {
                        minMovesForCars = CarsOnPark[i].getMoves();
                    }
                    cntOfNewTrucks--;
                }
            }


            if (cntOfNewCars > 0) {   //   <- если нет мест для легковых машин
                System.out.println(
                        "The park places for cars is full\n" +
                                "Place will appear in " + minMovesForCars + " moves");
            }

            if (cntOfNewTrucks > 0) { //   <- если нет мест для грузовых машин
                System.out.println("The park places for trucks is full\n" +
                        "Places will appear in " + minMovesForTrucks + " moves");
            }
            letsGo = false;
            showCommand();

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ К о м а н д ы ~~~~~~~~~~~~~~~~~~~~~~~~~~
            boolean doIt =true;
            do {
                String item = sc.next();
                switch (item) {
                    case "1" -> {
                        letsGo = true;
                        doIt = false;
                        System.out.println("The move is made!");
                    }
                    case "2" -> {
                        int tempValue = emptyPlaces(CarsOnPark) + emptyPlaces(TrucksOnPark);
                        System.out.println("Free places: " + tempValue);
                        System.out.println("Taken places: " + (newPark.getCapacity() - tempValue));
                        System.out.println("Place for car will appear in: " + minMovesForCars + " moves");
                        System.out.println("Place for truck will appear in: " + minMovesForTrucks + " moves");
                        showCommand();
                    }
                    case "3" -> {
                        detailedInformation(CarsOnPark, "car");
                        detailedInformation(TrucksOnPark, "truck");
                        showCommand();
                    }
                    case "4" -> {
                        Arrays.fill(CarsOnPark, null);
                        Arrays.fill(TrucksOnPark, null);
                        System.out.println("The parking lot cleared");
                        showCommand();
                    }
                    case "5" -> doIt = false;
                    default -> {
                        System.out.println("Enter number for 1 to 5");
                        showCommand();
                    }
                }
            }
            while (doIt);
        }
    }


// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ М е т о д ы ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    public static void detailedInformation (Cars[] cars, String type){
        System.out.println("~~~~~~~~~~~~~ "+type+"s"+" ~~~~~~~~~~~~~~");
        for (int i = 0; i<cars.length; i++) {
            System.out.println(i + 1 + ")");
            if (cars[i] != null) {
                System.out.println(
                        "Type of place: " + cars[i].getType() +"\n" +
                                "Number of auto: " + cars[i].getNumber()+"\n" +
                                "Life time: " + cars[i].getMoves()+ "\n");
            } else{
                System.out.println("Type of place: "+ type);
                System.out.println("Place is empty"+"\n");
            }
        }
    }


    public static void takePlace(Cars[] cars,String type){
        for(int i = 0; i<cars.length; i++){
            if (cars[i] == null) {

                if(type.equals("car") && cntOfNewCars>0) {
                    cars[i] = new Cars(1 + (int) (Math.random() * 9), uniqueNumber(), type);
                    if(cars[i].getMoves() < minMovesForCars) {
                        minMovesForCars = cars[i].getMoves();
                    }
                    cntOfNewCars--;
                }

                if(type.equals("truck")&&cntOfNewTrucks>0){
                    cars[i] = new Cars(1 + (int) (Math.random() * 9), uniqueNumber(), type);
                    if(cars[i].getMoves() < minMovesForTrucks) {
                        minMovesForTrucks= cars[i].getMoves();
                    }
                    cntOfNewTrucks--;
                }
            }
        }
    }


    public static int checkMovesAndGetMinOfThem(Cars[] cars, int min,String type) {
        for (int i = 0; i < cars.length; i++) {
            if (cars[i] != null) {
                if (cars[i].getMoves() > 0) {
                    cars[i].setMoves(cars[i].getMoves() - 1);

                    if(i > 1 && cars[i-1] != null){
                        if (type.equals("car") && cars[i].getType().equals("truck")) {
                            if (cars[i].getNumber() == cars[i - 1].getNumber()) {
                                cars[i].setMoves(cars[i].getMoves() + 1); // исправление бага с 2 - ым вычетом ходов
                            }
                        }
                    }

                    if (cars[i].getMoves() == 0) {
                        if((type.equals("car") && cars[i].getType().equals("truck"))){
                            cars[i+1] = null;
                        }
                        cars[i] = null;
                    }
                }
                min = minOfCarsMoves(cars);
            }
        }
        return min;
    }


    public static void showCommand(){
        System.out.println(
                "Choose item:\n" +
                        "1) next move\n" +
                        "2) info about places\n" +
                        "3) detailed information\n"+
                        "4) clear car\n"+
                        "5) exit");
    }


    public static int emptyPlaces(Cars[] cars){
        int empty =0;
        for(Cars x:cars){
            if(x==null){
                empty++;
            }
        }
        return empty;
    }


    public static int minOfCarsMoves(Cars[] cars){
        int min = 11;
        for (Cars car : cars) {
            if (car != null) {
                if (car.getMoves() < min) {
                    min = car.getMoves();
                }
            }
        }
        return min;
    }

    public static int uniqueNumber(){
        boolean[] checkNumber = new boolean[10000];
        int number = 1000+ (int) (Math.random()*8999);

        while(checkNumber[number]) {
            number = 1000+ (int) (Math.random()*8999);
        }
        checkNumber[number] = true;
        return number;
    }
}
