package file_traversal;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileManager {
    public static void main(String[] args) {
        FileManager fileManager = new FileManager();
        fileManager.mkSomething("C:\\Users\\user\\Downloads\\Telegram Desktop");
    }

    public void mkSomething(String src) {
        Path path = Paths.get(src);
        try {
            Files.createFile(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
