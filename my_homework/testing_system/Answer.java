package testing_system;

public class Answer {
    private String textOfAnswer;
    private double reward;
    private int id;
    private boolean isSelected;
    private boolean isTrueAnswer;


    public Answer(String textOfAnswer, int id) {
        this.textOfAnswer = textOfAnswer;
        this.id = id;
    }

    public boolean isTrueAnswer() {
        return isTrueAnswer;
    }

    public void makeTrueAnswer() {
        this.isTrueAnswer = true;
    }

    public boolean isSelected() {
        return isSelected;
    }

    @Override
    public String toString() {
        return id + ") " + textOfAnswer;
    }

    public void makeSelected() {
        this.isSelected = true;
    }

    public void makeNotSelected() {
        this.isSelected = false;
    }


    public void setReward(double reward) {
        this.reward = reward;
    }

    public String getTextOfAnswer() {
        return textOfAnswer;
    }


    public int getId() {
        return id;
    }

    public double getReward() {
        return reward;
    }
}

