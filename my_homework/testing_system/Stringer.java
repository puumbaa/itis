package testing_system;

import java.util.Scanner;

public class Stringer {

    private static Scanner sc = new Scanner(System.in);

    public static String enterString() {
        String str = sc.nextLine();
        if (str.equals("")) return sc.nextLine();
        else return str;
    }
}
