package testing_system;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Test {
    private long time;
    private double appraisal = 0;
    private final List<Question> listOfQuestions = new ArrayList<>();
    static Scanner sc = new Scanner(System.in);
    static int targetIndex = 0;


    public static void main(String[] args) {
        Test test = new Test();
        while (true) {
            test.startMenu();
        }
    }


    public void startMenu() {
        System.out.println("1) Добавить вопрос" + "\n" + "2) Начать тестирование" + "\n" + "3) Выйти ");

        String userChoice = Stringer.enterString();
        switch (userChoice) {
            case "1" -> addQuestion();
            case "2" -> beginTesting();
            case "3" -> System.exit(0);
            default -> System.out.println("Вы ввели неправильное значение");
        }
    }

    public void addQuestion() {
        Question question = new Question();

        System.out.println("Введите текст вопроса: ");

        question.setTextOfQuestion(Stringer.enterString());
        System.out.println("Введите стоимость вопроса: ");
        double val = sc.nextDouble();

        while (val <= 0){
            System.out.println("Введите значение > 0");
            val = sc.nextDouble();
        }
        question.setValue(val);


        System.out.println("Введите кол-во ответов: ");
        int cntOfAnswers = sc.nextInt();
        while (cntOfAnswers < 1) {
            System.out.println("Введите положительное значение");
            cntOfAnswers = sc.nextInt();
        }
        System.out.println("Наберите текст ответов: ");
        for (int i = 0; i < cntOfAnswers; i++) {
            System.out.print((i + 1) + ") ");
            question.getListOfAnswers().add(new Answer(Stringer.enterString(), i + 1));
        }

        System.out.println("Введите кол-во верных ответов: ");
        int cntOfTrueAnswer = sc.nextInt();

        while ((cntOfTrueAnswer > cntOfAnswers) || (cntOfTrueAnswer <= 0) ){
            System.out.println("Вы ввели некорректное значение");
            cntOfTrueAnswer=sc.nextInt();
        }


        question.setCntOfTrueAnswers(cntOfTrueAnswer);
        System.out.println("Введите номера верных ответов: ");


        for (int i = 0; i < cntOfTrueAnswer; i++) {
            int userIntroducedId = sc.nextInt();
            while (userIntroducedId > cntOfAnswers || userIntroducedId<1){
                System.out.println("Вы ввели некорректное значение");
                userIntroducedId=sc.nextInt();
            }

            for (int j = 0; j < cntOfAnswers; j++) {
                if (question.getListOfAnswers().get(j).getId() == userIntroducedId) {
                    question.getListOfAnswers().get(j).makeTrueAnswer();
                    question.getListOfAnswers().get(j).setReward(question.getValue() / cntOfTrueAnswer);
                }
            }
        }

        listOfQuestions.add(question);
        startMenu();
    }

    public void beginTesting() {

        if(listOfQuestions.size()==0) {
            System.out.println("Вопросов нет, добавьте их в главном меню: ");
            startMenu();
        }

        Question.clearQuestions(listOfQuestions);


        System.out.println("Введите кол-во вопросов: ");
        int cntOfQuestions = sc.nextInt();
        while (cntOfQuestions > listOfQuestions.size()) {
            System.out.println("Введено слишком большое значение");
            cntOfQuestions = sc.nextInt();
        }

        for (int i = 0; i < cntOfQuestions; i++) {
            Question randomQuestion = Question.getRandomQuestion(listOfQuestions,cntOfQuestions);
            System.out.println(randomQuestion.toString());

            time = System.currentTimeMillis();

            System.out.println("Введите номера верных вариантов: ");
            String[] userAnswers = userEnterOwnAnswers(randomQuestion);

            for (String s : userAnswers) {
                int selectedAnswerId = Integer.parseInt(s);


                randomQuestion.getListOfAnswers().get(selectedAnswerId - 1).makeSelected();

                if (randomQuestion.getListOfAnswers().get(selectedAnswerId - 1).isTrueAnswer()) {

                    appraisal += randomQuestion.getListOfAnswers().get(selectedAnswerId - 1).getReward();
                    randomQuestion.setCntOfTrueAnswers(randomQuestion.getCntOfTrueAnswers() - 1);

                }
            }
            if (randomQuestion.getCntOfTrueAnswers() == 0) {
                randomQuestion.makeSolvedCorrectly();
            }
        }
        showResults();
    }

    public void showResults() {
        int maxVal = 0;
        for (Question x : listOfQuestions) {
            maxVal += (int) x.getValue();
        }
        System.out.println("Набрано " + appraisal + " баллов из " + maxVal);
        System.out.println("Тест выполнен за: " + (System.currentTimeMillis() - time) / 1000 + " sec");

        for (Question x : listOfQuestions) {

            if (!x.isSolvedCorrectly()) {
                System.out.println("Неправильный ответ на вопрос: ");
                System.out.println(x.getTextOfQuestion());

                System.out.println("Ожидалось: ");
                for (int i = 0; i < x.getListOfAnswers().size(); i++) {
                    if (x.getListOfAnswers().get(i).isTrueAnswer()) {
                        System.out.println(x.getListOfAnswers().get(i).toString());
                    }
                }

                System.out.println("Получено: ");
                for (int i = 0; i < x.getListOfAnswers().size(); i++) {
                    if (x.getListOfAnswers().get(i).isSelected()) {
                        System.out.println(x.getListOfAnswers().get(i).toString());
                    }
                }
            }
        }
        nextStep();

    }

    public String[] userEnterOwnAnswers(Question question) {
        String[] str = Stringer.enterString().split(" ");
        for (String s : str) {
            int num = Integer.parseInt(s);
            if (num > question.getListOfAnswers().size() || num < 1) {
                System.out.println(("Вы ввели некорректное значение"));
                str = userEnterOwnAnswers(question);
            }
        }
        return str;
    }

    public void nextStep(){
        System.out.println("\n" + "Что дальше?" + "\n" + "1) Пройти еще один тест" + "\n"
                + "2) Вернуть в главное меню" + "\n"
                + "3) Выйти");

        int select = sc.nextInt();
        appraisal = 0;
        switch (select) {
            case 1 -> beginTesting();
            case 2 -> startMenu();
            case 3 -> System.exit(0);
        }
    }

}
