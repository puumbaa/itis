package testing_system;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Question {

    private List<Answer> listOfAnswers = new ArrayList<>();
    private String textOfQuestion;
    private double value;
    private int cntOfTrueAnswers;
    private boolean isSolvedCorrectly;


    public void makeSolvedCorrectly() {
        this.isSolvedCorrectly = true;
    }

    public void setCntOfTrueAnswers(int cntOfTrueAnswers) {
        this.cntOfTrueAnswers = cntOfTrueAnswers;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public List<Answer> getListOfAnswers() {
        return listOfAnswers;
    }

    public String getTextOfQuestion() {
        return textOfQuestion;
    }

    public void setTextOfQuestion(String textOfQuestion) {
        this.textOfQuestion = textOfQuestion;
    }

    public static Question getRandomQuestion(List<Question> listOfQuestions, int size) {// Выбор случайного вопроса
        int[] arrOfRandomInd = new int[size];
        for (int i = 0; i < size; i++) {
            arrOfRandomInd[i] = i;
        }

        Random random = new Random();
        for (int i = 0; i < size; i++) {
            int randomInd = random.nextInt(i + 1);
            int temp = arrOfRandomInd[i];
            arrOfRandomInd[i] = arrOfRandomInd[randomInd];
            arrOfRandomInd[randomInd] = temp;
        }
        Question randomQuestion = listOfQuestions.get(arrOfRandomInd[Test.targetIndex]);
        Test.targetIndex++;
        return randomQuestion;
    }


    public String showVariants(List<Answer> answers) { // Вывод всех вариантов
        String s = "";
        for (Answer x : answers) {
            s = s.concat((x.getId()) + ") " + x.getTextOfAnswer() + "\n");
        }
        return s;
    }

    @Override
    public String toString() {
        return textOfQuestion + "  ( стоимость: " + (int) value + " )" + "\n" + showVariants(this.getListOfAnswers());
    }

    public int getCntOfTrueAnswers() {
        return cntOfTrueAnswers;
    }

    public boolean isSolvedCorrectly() {
        return isSolvedCorrectly;
    }

    public static void clearQuestions(List<Question> list) {
        for (int i = 0; i < list.size(); i++) {
            for (int j = 0; j < list.get(i).getListOfAnswers().size(); j++) {
                list.get(i).getListOfAnswers().get(j).makeNotSelected();
            }
        }
    }

}
